$(document).ready(function(){
	$("a").on("click", function(event){

		var target = this.hash;
		if(target !== ""){
			event.preventDefault();
			$("html, body").animate({
				scrollTop: $(target).offset().top
			}, 1000, function(){
				window.location.hash = target;
			});
		}
	});
});